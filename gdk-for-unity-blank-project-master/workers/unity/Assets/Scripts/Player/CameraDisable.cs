﻿using Assets.Scripts.Player;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraDisable : MonoBehaviour
{

    PlayerInput input;
    Camera camera;
    private void Start()
    {
        PlayerInput input = gameObject.GetComponent<PlayerInput>();
        Camera camera = gameObject.GetComponentInChildren<Camera>();
        Debug.LogError("Assigned variables");


        if(input.enabled == false)
        {
            camera.gameObject.SetActive(false);
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (camera == null || input == null)
            return;
        camera.gameObject.SetActive(input.enabled);
    }
}
