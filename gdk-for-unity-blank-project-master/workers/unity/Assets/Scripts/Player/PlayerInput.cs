﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Improbable.Gdk.GameObjectRepresentation;
using Improbable;
using Improbable.Player;
namespace Assets.Scripts.Player
{
    [WorkerType("UnityClient")]
    public class PlayerInput : MonoBehaviour
    {
        [Require] private PlayerControls.Requirable.Writer inputWriter;
        [Require] private Position.Requirable.Reader positionReader;
        [Require] private PlayerInventory.Requirable.Reader ArcitReader;

        float updateTime = .1f;
        float lastUpdate = 0f;
        bool onResource = false;
        GameObject currentResource;


        // Start is called before the first frame update
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {
            if (lastUpdate <= updateTime)
            {
                lastUpdate += Time.deltaTime;
                return;
            }

            lastUpdate = 0;

            float x = Input.GetAxis("Horizontal");
            float y = Input.GetAxis("Vertical");

            inputWriter.SendMovementUpdate(new MovementUpdate(x, y));
            Debug.LogError("Sent");


            if (Input.GetKeyDown(KeyCode.E))
            {
                inputWriter.SendHarvest(new Harvest());
            }

            
        }
        void OnPositionUpdate(Coordinates update)
        {
            Vector3 newPos = new Vector3((float)update.X, (float)update.Y, (float)update.Z);
            gameObject.transform.position = newPos;
        }

        private void OnEnable()
        {
            Debug.LogError("Launching");
            positionReader.CoordsUpdated += OnPositionUpdate;
            
        }


        private void OnTriggerEnter(Collider collision)
        {
            if (collision.gameObject.tag == "Resource")
            {
                onResource = true;
                currentResource = collision.gameObject;
            }
        }

        private void OnTriggerExit(Collider collision)
        {
            if (collision.gameObject.tag == "Resource")
            {
                onResource = false;
            }
        }

        void OnHarvestUpdate(Harvest HarvestUpdate)
        {
            if (onResource)
            {
                Destroy(currentResource);
                
            }
        }
    }
}