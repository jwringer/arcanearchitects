﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Improbable.Player;
using Improbable;
using Improbable.Gdk.GameObjectRepresentation;


namespace Assets.Scripts.Player
{



    //[WorkerType("UnityClient")]
    public class PlayerMovement : MonoBehaviour
    {



        [Require] private PlayerControls.Requirable.Reader inputReader;
        [Require] private Position.Requirable.Writer positionWriter;
        [Require] private PlayerInventory.Requirable.Writer playerResource;

        float speed = .5f;
        bool onResource = false;
        GameObject currentResource;




        void OnControlUpdate(MovementUpdate update)
        {
            Vector2 control = new Vector2(update.X, update.Y);
            control.Normalize();
            control = control * speed;

            Vector3 newPos = this.transform.position;
            newPos.x = newPos.x + control.x;
            newPos.z = newPos.z + control.y;
            this.transform.position = newPos;

            Coordinates serverPos = new Coordinates((double)newPos.x, (double)newPos.y, (double)newPos.z);

            positionWriter.Send(new Position.Update
            {
                Coords = serverPos
            });

        }

        private void OnEnable()
        {
            inputReader.OnMovementUpdate += OnControlUpdate;
            inputReader.OnHarvest += OnHarvestUpdate;
        }

            // Start is called before the first frame update
            void Start()
        {

        }

        private void OnTriggerEnter(Collider collision)
        {
            if (collision.gameObject.tag == "Resource")
            {
                onResource = true;
                currentResource = collision.gameObject;
            }
        }

        private void OnTriggerExit(Collider collision)
        {
            if (collision.gameObject.tag == "Resource")
            {
                onResource = false;
            }
        }

        void OnHarvestUpdate(Harvest HarvestUpdate)
        {
            if (onResource)
            {
                Destroy(currentResource);
                playerResource.Send(new PlayerInventory.Update { Arcite = playerResource.Data.Arcite + 10 });
            }
        }

        // Update is called once per frame
        void Update()
        {

        }
    }
}